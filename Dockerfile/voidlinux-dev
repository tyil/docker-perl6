FROM voidlinux/voidlinux:latest AS build

COPY dev-bin/build.sh /root/build.sh

RUN xbps-install -Syu
RUN xbps-install -Sy base-devel bash git
RUN sh /root/build.sh moarvm nqp rakudo zef

FROM voidlinux/voidlinux:latest

LABEL schema-version="1.0"
LABEL maintainer="Patrick Spek <p.spek@tyil.work>"
LABEL description="Development container for Perl 6 applications, built nightly"
LABEL vcs-url="https://gitlab.com/tyil/docker-perl6"

ENV PATH=/usr/local/share/perl6/site/bin:$PATH
ENV PERL6LIB=/app/lib

WORKDIR /app

COPY --from=build /bin /bin
COPY --from=build /lib /lib
COPY --from=build /sbin /sbin
COPY --from=build /usr /usr

CMD ["perl6"]

# vim: ft=dockerfile
