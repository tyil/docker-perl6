#! /usr/bin/env sh

set -e

export MOAR_BRANCH=master
export MOAR_REPO=https://github.com/moarvm/moarvm.git
export NQP_BRANCH=master
export NQP_REPO=https://github.com/perl6/nqp.git
export RAKUDO_BRANCH=master
export RAKUDO_REPO=https://github.com/rakudo/rakudo.git

main()
{
	for target in $*
	do
		"$target"
	done
}

moarvm()
{
	printf "Building MoarVM from %s (%s)\n" "$MOAR_REPO" "$MOAR_BRANCH" >&2

	cd -- "$(mktemp -d)"

	git clone --branch "$MOAR_BRANCH" "$MOAR_REPO" .

	perl ./Configure.pl --prefix=/usr/local
	make
	make install
}

nqp()
{
	printf "Building NQP from %s (%s)\n" "$NQP_REPO" "$NQP_BRANCH" >&2

	cd -- "$(mktemp -d)"

	git clone --branch "$NQP_BRANCH" "$NQP_REPO" .

	perl ./Configure.pl --backend=moar --prefix=/usr/local
	make
	make install
}

rakudo()
{
	printf "Building Rakudo from %s (%s)\n" "$RAKUDO_REPO" "$RAKUDO_BRANCH" >&2

	cd -- "$(mktemp -d)"

	git clone --branch "$RAKUDO_BRANCH" "$RAKUDO_REPO" .

	perl ./Configure.pl --backend=moar --prefix=/usr/local
	make
	make install
}

zef()
{
	cd -- "$(mktemp -d)"

	git clone https://github.com/ugexe/zef.git .

	PERL6LIB="$(pwd)" perl6 bin/zef install .
}

main "$@"
