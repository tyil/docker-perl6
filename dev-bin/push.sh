#! /usr/bin/env sh

main()
{
	docker push "$1-latest"

	# Only tag/push a daily image if requested. In practice, the GitLab build
	# schedule sets this environment variable, so only daily builds from the
	# schedule make the images with date tags. Everything else builds -latest.
	if [ -n "$INCLUDE_DAILY" ]
	then
		date=$(date "+%F")

		docker tag "$1-latest" "$1-$date"
		docker push "$1-$date"
	fi
}

main "$@"
